import { useState } from 'react';
import {
  Box,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Button,
  Container,
  VStack,
  Select,
  InputGroup,
  InputRightElement,
  FormErrorMessage,
  Link,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from '@chakra-ui/react';
import { HiEye, HiEyeOff } from 'react-icons/hi';
import { useNavigate } from 'react-router-dom';
import Lottie from 'lottie-react';
import animationData from '../../package.json';

const SignUpPage = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [responsibility, setResponsibility] = useState('');
  const [otp, setOtp] = useState('');
  const [showOtp, setShowOtp] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [openSuccessModal, setOpenSuccessModal] = useState(false);

  const [formErrors, setFormErrors] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    responsibility: '',
  });

  const handleSignUp = () => {
    console.log('First Name:', firstName);
    console.log('Last Name:', lastName);
    console.log('Email:', email);
    console.log('Password:', password);
    console.log('Responsibility:', responsibility);
    // Add your sign-up logic here
    onOpen();
  };

  const isFormValid =
    firstName !== '' &&
    lastName !== '' &&
    email !== '' &&
    password !== '' &&
    responsibility !== '';

  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  const handlePasswordChange = (e) => {
    const value = e.target.value;
    setPassword(value);
    setFormErrors((prevErrors) => ({
      ...prevErrors,
      password:
        value.length < 8
          ? 'Password should be minimum 8 characters'
          : value.includes(' ')
          ? 'Password cannot include spaces'
          : '',
    }));
  };

  const handleFieldChange = (field, value) => {
    if (value === '') {
      setFormErrors((prevErrors) => ({
        ...prevErrors,
        [field]: `This field cannot be empty`,
      }));
    } else {
      setFormErrors((prevErrors) => ({
        ...prevErrors,
        [field]: '',
      }));
    }
  };
 const navigate = useNavigate();
 const handleToggleOtp = () => {
  setShowOtp(!showOtp);
};

const handleVerifyOtp = () => {
  // Perform OTP verification logic here
  // Redirect to dashboard after successful verification
  onClose();
setOpenSuccessModal(true)
  setTimeout(()=>{

     navigate("/")
  },4000);
 
  // Redirect logic goes here
};

  return (
    <Box bg="gray.200" minH="100vh" py={8}>
      <Container
        maxW="xl"
        bg="white"
        borderRadius="lg"
        p={4}
        boxShadow="lg"
        mt={150}
      >
        <VStack spacing={4} align="center">
          <Heading size="lg" textAlign="center">
            Create Your Account
          </Heading>
          <FormControl isInvalid={formErrors.firstName !== ''} w="full">
            <FormLabel>First Name</FormLabel>
            <Input
              type="text"
              value={firstName}
              onChange={(e) => {
                setFirstName(e.target.value);
                handleFieldChange('firstName', e.target.value);
              }}
              placeholder="Enter your first name"
            />
            <FormErrorMessage>{formErrors.firstName}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.lastName !== ''} w="full">
            <FormLabel>Last Name</FormLabel>
            <Input
              type="text"
              value={lastName}
              onChange={(e) => {
                setLastName(e.target.value);
                handleFieldChange('lastName', e.target.value);
              }}
              placeholder="Enter your last name"
            />
            <FormErrorMessage>{formErrors.lastName}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.email !== ''} w="full">
            <FormLabel>Email</FormLabel>
            <Input
              type="email"
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
                handleFieldChange('email', e.target.value);
              }}
              placeholder="Enter your email"
            />
            <FormErrorMessage>{formErrors.email}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.password !== ''} w="full">
            <FormLabel>Password</FormLabel>
            <InputGroup>
              <Input
                type={passwordVisible ? 'text' : 'password'}
                value={password}
                onChange={handlePasswordChange}
                placeholder="Enter your password"
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={togglePasswordVisibility}>
                  {passwordVisible ? <HiEyeOff /> : <HiEye />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>{formErrors.password}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.responsibility !== ''} w="full">
            <FormLabel>Responsibility</FormLabel>
            <Select
              placeholder="Select your responsibility"
              value={responsibility}
              onChange={(e) => {
                setResponsibility(e.target.value);
                handleFieldChange('responsibility', e.target.value);
              }}
            >
              <option value="option1">Option 1</option>
              <option value="option2">Option 2</option>
            </Select>
            <FormErrorMessage>{formErrors.responsibility}</FormErrorMessage>
          </FormControl>
          <Button
            colorScheme="teal"
            onClick={handleSignUp}
            isFullWidth
            disabled={!isFormValid}
          >
            Sign Up
          </Button>
          <Heading size="sm"> Already Have an Account ? <Link onClick={() => navigate("/")}>Login Here</Link></Heading>

        </VStack>
      </Container>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Verify OTP</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl id="otp">
              <FormLabel>OTP</FormLabel>
              <InputGroup>
                <Input
                  type={showOtp ? 'text' : 'password'}
                  value={otp}
                  onChange={(e) => setOtp(e.target.value)}
                />
                <InputRightElement width="4.5rem">
                  <Button
                    h="1.75rem"
                    size="sm"
                    onClick={handleToggleOtp}
                    variant="ghost"
                    aria-label={showOtp ? 'Hide OTP' : 'Show OTP'}
                    leftIcon={showOtp ? <HiEyeOff /> : <HiEye />}
                  />
                </InputRightElement>
              </InputGroup>
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="teal" mr={3} onClick={handleVerifyOtp}>
              Verify
            </Button>
            <Button variant="ghost" onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      {/* <Modal isOpen={isVerifiedOpen} onClose={onVerifiedClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Verified</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack spacing={4}>
              <Lottie animationData={animationData} autoplay loop />
              <p>Your account has been verified!</p>
            </VStack>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" onClick={onVerifiedClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal> */}
        
       {openSuccessModal == true ? (
        <Modal isOpen={true} size="xl">
          <ModalOverlay />
          <ModalContent>
            <ModalHeader
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              We have Successfully Created Your Account
            </ModalHeader>
            <ModalBody>
              <lottie-player
                src="https://assets10.lottiefiles.com/packages/lf20_8utzooUGTM.json"
                background="transparent"
                speed="1"
                autoplay
              ></lottie-player>
            </ModalBody>
          </ModalContent>
        </Modal>
      ) : (
        ''
      )}
    
    </Box>
  );
};

export default SignUpPage;
