import React, { useState, useEffect } from "react";
import "../App.css"
import {
  Box,
  Button,
  ChakraProvider,
  Container,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
} from "@chakra-ui/react";
import { Link as ReachLink, useNavigate } from "react-router-dom";
import axios from 'axios';
import pic from "../Assets/user.jpg"
function DashboardPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [events, setEvents] = useState([]);
  const [selectedTemplate, setSelectedTemplate] = useState("");
  const [isRefreshLoading, setIsRefreshLoading] = useState(true);

  const navigate = useNavigate();
  const [openSuccessModal, setOpenSuccessModal] = useState(false);
  // const fetchEvents = async () => {
  //   try {
  //     const response = await fetch(
  //       "https://jsonplaceholder.typicode.com/posts"
  //     );
  //     const data = await response.json();
  //     setEvents(data);
  //     setIsRefreshLoading(true)
  //   } catch (error) {
  //     console.error("Error fetching events:", error);
  //   }
  // };

  useEffect(() => {
    fetchEvents();
  }, []);


  const fetchEvents = async () => {
    try {
      const response = await axios.get(
        'https://jsonplaceholder.typicode.com/posts'
      );
      const data = response.data;
      setEvents(data);
      setIsRefreshLoading(true);
    } catch (error) {
      console.error('Error fetching events:', error);
    }
  };

  // Function to handle form submission
  const handleSubmit = () => {
    // Process form data and create event
    
    setOpenSuccessModal(true);
    
    setTimeout(() => {
      setOpenSuccessModal(false);
      closeModal();
      navigate('/dashboard')
    },4500);
  };

  const [modOpen, setModOpen] = useState(false);

  const openModal = () => {
    setModOpen(true);
  };

  const closeModal = () => {
    setModOpen(false);
  };

  return ( 
      <Box bg="gray.100">

        <Flex
          bg="teal.500"
          p={4}
          justifyContent="space-around"
          alignItems="center"
        >
          <Box>
            <Button colorScheme="teal" mr={4}>
              Dashboard
            </Button>
            <Button colorScheme="teal" mr={4} onClick={openModal}>
              Create Event
            </Button>
            <Button colorScheme="teal">Event Templates</Button>
          </Box>
          <Box>
            {/* Profile dropdown */}
            <Menu>
              <MenuButton as={Button} variant="link">
                <img
                  src={pic}
                  alt="Profile"
                  id="dashboardUserPic"
                />
              </MenuButton>
              <MenuList>
                <MenuItem onClick={() => navigate("/")}>
                  Logout
                </MenuItem>
              </MenuList>
            </Menu>
          </Box>
        </Flex>

        {/* Dashboard */}
        <Container maxW="container.xl" py={8}>
          <Button colorScheme="teal" onClick={fetchEvents}>
            Refresh Here
          </Button>
          <Table variant="simple" mt={4}>
            <Thead>
              <Tr>
                <Th>SL No.</Th>
                <Th>Name</Th>
                <Th>Completed</Th>
              </Tr>
            </Thead>
            <Tbody>
              {events.map((event) => (
                <Tr key={event.id}>
                  <Td>{event.id}</Td>
                  <Td>{event.title}</Td>
                  <Td>{event.completed}</Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </Container>




        <Modal isOpen={modOpen} onClose={closeModal}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Create Event</ModalHeader>
            <ModalBody>
              <FormControl>
                <FormLabel>Enter Event Name</FormLabel>
                <Input type="text" size="md" /> <br />
                <FormLabel>Enter Event Type</FormLabel>
                <Input type="text" size="md" /> <br />
                <FormLabel>Select Start Date</FormLabel>
                <Input type="date" size="md" /> <br />
                <FormLabel>Select End Date</FormLabel>
                <Input type="date" size="md" /> <br />
                <FormLabel>Select Template</FormLabel>
                <Select
                  value={selectedTemplate}
                  onChange={(e) => setSelectedTemplate(e.target.value)}
                >
                  <option value="template1">Template 1</option>
                  <option value="template2">Template 2</option>
                  <option value="template3">Template 3</option>
                </Select>
              </FormControl>
              {/* Additional form fields */}
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="teal" mr={3} onClick={handleSubmit}>
                Create
              </Button>
              <Button onClick={closeModal}>Cancel</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>




        {openSuccessModal == true ? (
          <Modal isOpen={true}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                Event Created
              </ModalHeader>
              <ModalBody>
                <lottie-player
                  src="https://assets9.lottiefiles.com/packages/lf20_rtxcgnqq.json"
                  background="transparent"
                  speed="1"
                  autoplay
                ></lottie-player>
              </ModalBody>
            </ModalContent>
          </Modal>
        ) : (
          " "
        )}
      </Box>
  );
}

export default DashboardPage;
