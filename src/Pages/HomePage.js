import { Heading } from '@chakra-ui/react'
import React from 'react'

function HomePage() {
  return (
    <Heading as="h1">HomePage</Heading>
  )
}

export default HomePage;