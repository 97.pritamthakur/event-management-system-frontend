import React, { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, InputGroup, InputRightElement, VStack, Heading, Flex } from '@chakra-ui/react';
import { HiEye, HiEyeOff } from 'react-icons/hi';

const SetPassword = () => {
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('New Password:', newPassword);
    console.log('Confirm Password:', confirmPassword);
  };

  const handleToggleNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };

  const handleToggleConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <Box bg="gray.200" minH="100vh" py={8}>
   <Heading display="flex" justifyContent="center"> Forgot Password</Heading>

      <form onSubmit={handleSubmit}>
        <VStack 
           maxW="md"
           bg="white"
           borderRadius="lg"
           p={4}
           mt={200}
           boxShadow="lg"
           mx={"auto"}
        spacing={4}>
          <FormControl id="newPassword">
            <FormLabel>New Password</FormLabel>
            <InputGroup>
              <Input
                type={showNewPassword ? 'text' : 'password'}
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
              />
              <InputRightElement width="4.5rem">
                <Button
                  h="1.75rem"
                  size="sm"
                  onClick={handleToggleNewPassword}
                  variant="ghost"
                  aria-label={showNewPassword ? 'Hide password' : 'Show password'}
                  leftIcon={showNewPassword ? <HiEyeOff /> : <HiEye />}
                />
              </InputRightElement>
            </InputGroup>
          </FormControl>

          <FormControl id="confirmPassword">
            <FormLabel>Confirm Password</FormLabel>
            <InputGroup>
              <Input
                type={showConfirmPassword ? 'text' : 'password'}
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <InputRightElement width="4.5rem">
                <Button
                  h="1.75rem"
                  size="sm"
                  onClick={handleToggleConfirmPassword}
                  variant="ghost"
                  aria-label={showConfirmPassword ? 'Hide password' : 'Show password'}
                  leftIcon={showConfirmPassword ? <HiEyeOff /> : <HiEye />}
                />
              </InputRightElement>
            </InputGroup>
          </FormControl>

          <Button type="submit" colorScheme="blue">Submit</Button>
        </VStack>
      </form>
    </Box>
  );
};

export default SetPassword;
