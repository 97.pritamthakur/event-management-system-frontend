import { Box, Button, FormControl, FormLabel, Heading, Input, VStack, Link } from "@chakra-ui/react";
import "../App.css"
import React, { useState} from "react";

import { useNavigate } from 'react-router-dom';

import { toast } from "react-toastify";


import { Container } from "react-bootstrap";

function ForgotPassword() {


  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);


  const handleSubmit = (e) => {
    e.preventDefault();
    
    setIsLoading(true);

    console.log("Forgot password request submitted!");
    setTimeout(() => {
        setIsLoading(false);
      }, 1000)
    setMessage("Email Sent to Your Registered Mail Id");
  };

  return (
    <Box bg="gray.200" minH="100vh" py={8} >
      <Heading  display="flex" justifyContent="center"> Forgot Password</Heading>
       
        <VStack
           maxW="md"
           bg="white"
           borderRadius="lg"
           p={4}
           mt={200}
           boxShadow="lg"
           mx={"auto"}
        as="form" onSubmit={handleSubmit} spacing={4}>
        <FormControl id="email" isRequired>
          <FormLabel>Email:</FormLabel>
          <Input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormControl>

        <Button type="submit" colorScheme='teal'>Submit</Button>
        <toast
        position="top-right"
        autoClose={1000}
        closeOnEscape={true}
        newestOnTop={true}
        id="toast"
      >
        {message}
      </toast>
        <br />
        <Heading size="sm"> Already Have an Account ? <Link onClick={() => navigate("/")}>Login Here</Link></Heading>
      </VStack>     
    </Box>
  );
}

export default ForgotPassword;
