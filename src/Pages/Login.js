import { useState } from 'react';
import {
  Box,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Button,
  Container,
  VStack,
  Select,
  Link,
  InputGroup,
  InputRightElement,
  FormErrorMessage,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from '@chakra-ui/react';
import { HiEye, HiEyeOff } from 'react-icons/hi';
import { useNavigate } from 'react-router-dom';
import ForgotPassword from './ForgotPassword';
const LoginPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [otp, setOtp] = useState('');
  const [showOtp, setShowOtp] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const [formErrors, setFormErrors] = useState({
    email: '',
    password: '',
  });
  const navigate = useNavigate();

  const handleLogin = () => {
    console.log('Email:', email);
    console.log('Password:', password);
    navigate('/dashboard')
  };

  const isFormValid = email !== '' && password !== '';

  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  const handlePasswordChange = (e) => {
    const value = e.target.value;
    setPassword(value);
    setFormErrors((prevErrors) => ({
      ...prevErrors,
      password:
        value.length < 8
          ? 'Password should be minimum 8 characters'
          : value.includes(' ')
          ? 'Password cannot include spaces'
          : '',
    }));
  };

  const handleFieldChange = (field, value) => {
    if (value === '') {
      setFormErrors((prevErrors) => ({
        ...prevErrors,
        [field]: `This field cannot be empty`,
      }));
    } else {
      setFormErrors((prevErrors) => ({
        ...prevErrors,
        [field]: '',
      }));
    }
  };



  return (
    <Box bg="gray.200" minH="100vh" py={8}>
      <Heading  as='h2' size='2xl'  display="flex" justifyContent="center"> Logo and Name</Heading>
      <Container
        maxW="md"
        bg="white"
        borderRadius="lg"
        p={4}
        mt={200}
        boxShadow="lg"
      >
        <VStack spacing={4} align="center">
          <Heading size="lg" textAlign="center">
            Login
          </Heading>
          <FormControl isInvalid={formErrors.email !== ''} w="full">
            <FormLabel>Email</FormLabel>
            <Input
              type="email"
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
                handleFieldChange('email', e.target.value);
              }}
              placeholder="Enter Your Email"
            />
            <FormErrorMessage>{formErrors.email}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={formErrors.password !== ''} w="full">
            <FormLabel>Password</FormLabel>
            <InputGroup>
              <Input
                type={passwordVisible ? 'text' : 'password'}
                value={password}
                onChange={handlePasswordChange}
                placeholder="Enter Your Password"
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={togglePasswordVisibility}>
                  {passwordVisible ?  <HiEye /> : <HiEyeOff />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>{formErrors.password}</FormErrorMessage>
          </FormControl>
          <Button
            colorScheme="teal"
            onClick={handleLogin}
            isFullWidth
            disabled={!isFormValid}
          >
            Login
          </Button>
        </VStack>
        <br />
        <Link fontWeight={600} fontSize={"16px"} onClick={() => navigate("/forgot-password")}>Forgot Password</Link>
        <Link fontWeight={600} fontSize={"16px"} onClick={() => navigate("/sign-up")} ml={"35%"}>Sign Up Here</Link>
      </Container>
    </Box>
  );
};

export default LoginPage;
