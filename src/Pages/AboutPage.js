import { Box, Container, Heading, Text } from "@chakra-ui/react";

const AboutPage = () => {
  return (
    <Box bg="gray.100" minH="100vh" py={10}>
      <Container maxW="container.lg" textAlign="center">
        <Heading as="h1" size="xl" mb={4}>
          About Event Management System
        </Heading>
        <Text fontSize="xl" color="gray.600" mb={8}>
          Simplify event planning and organization with our comprehensive
          system.
        </Text>
        <Box textAlign="left" bg="white" p={6} borderRadius="md">
          <Heading as="h2" size="lg" mb={4}>
            Our Mission
          </Heading>
          <Text fontSize="lg" color="gray.700" mb={4}>
            We are dedicated to providing a seamless event management experience
            to our users. Our goal is to simplify the planning and organization
            process, ensuring successful and memorable events.
          </Text>
          <Heading as="h2" size="lg" mb={4}>
            Features
          </Heading>
          <ul>
            <li>
              <Text fontSize="lg" color="gray.700">
                Streamlined event creation and management
              </Text>
            </li>
            <li>
              <Text fontSize="lg" color="gray.700">
                Attendee registration and ticketing
              </Text>
            </li>
            <li>
              <Text fontSize="lg" color="gray.700">
                Real-time analytics and reporting
              </Text>
            </li>
            <li>
              <Text fontSize="lg" color="gray.700">
                Communication tools for event promotion and updates
              </Text>
            </li>
            <li>
              <Text fontSize="lg" color="gray.700">
                Integration with popular payment gateways
              </Text>
            </li>
          </ul>
        </Box>
      </Container>
    </Box>
  );
};

export default AboutPage;
